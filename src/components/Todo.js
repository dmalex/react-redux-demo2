
import React from 'react'
import PropTypes from 'prop-types'

const Todo = ({ id, text, onClick }) => (
  <div onClick={onClick}>
    {id + 1}{'. '}{text}
  </div>
)

Todo.propTypes = {
  id: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired
}

export default Todo


const todos = (state = [], action) => {
  if (action.type === 'ADD_TODO') {
    return [
      ...state,
      {
        id: action.id,
        text: action.text
      }
    ]
  } else if (action.type === 'REMOVE_TODO') {
    // alert("remove")
    return state.filter(todo => todo.id !== action.id)
  } else {
    return state
  }
}
  
export default todos
